#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <string.h>
#include <stdint.h>
#include <png.h>
#include <time.h>
#include <unistd.h>

uint32_t *framebuffer;

void load_frame(int *fb, int n) {
	char *file_name = malloc(sizeof(char) * 18);

	asprintf(&file_name, "frames/out%d.png", n);

	printf("Opening frame %s...\n", file_name);

	FILE *fp = fopen(file_name, "rb");
	if(!fp) {
		perror("Can't open the frame");
		return;
	}

	png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if(!png) {
    	fclose(fp);
    	perror("Can't read png structure");
    	return;
    }

    png_infop info = png_create_info_struct(png);
    if(!info) {
    	fclose(fp);
    	png_destroy_read_struct(&png, NULL, NULL);
    	perror("Can't read png information");
    	return;
    }

    if(setjmp(png_jmpbuf(png))) {
    	fclose(fp);
    	png_destroy_read_struct(&png, &info, NULL);
    	perror("Can't read the frame");
    	return;
    }

    png_init_io(png, fp);
    png_read_info(png, info);

    int rowbytes = png_get_rowbytes(png, info);
    png_bytep *row_pointers = (png_bytep *) malloc(sizeof(png_bytep) * 1080);
    for(int y = 0; y < 1080; y++) {
    	row_pointers[y] = (png_byte *) malloc(rowbytes);
    }

    png_read_image(png, row_pointers);

    for(int y = 0; y < 1080; y++) {
    	png_bytep row = row_pointers[y];
    	for(int x = 0; x < 1440; x++) {
    		png_bytep px = &(row[x * 3]);
    		int pixel = (px[0] << 16) | (px[1] << 8) | (px[2]);
    		fb[x + y * 1440] = pixel;
    	}
    }

    png_destroy_read_struct(&png, &info, NULL);
    fclose(fp);
    free(row_pointers);
    for(int i = 0; i < 1080; i++) {
    	free(row_pointers[i]);
    }
}

int main(int argc, char *argv[]) {
	int fbfd = 0;
	struct fb_var_screeninfo vinfo;
	struct fb_fix_screeninfo finfo;
	long int screensize = 0;
	char *fbp = 0;

	int frame[1440 * 1080];

	fbfd = open("/dev/fb0", O_RDWR);
	if (fbfd == -1) {
		perror("Error opening framebuffer device");
		exit(1);
	}

	puts("The framebuffer device was opened successfully.");

	if (ioctl(fbfd, FBIOGET_FSCREENINFO, &finfo) == -1) {
		perror("Error reading fixed information");
		close(fbfd);
		exit(2);
	}

	if (ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo) == -1) {
		perror("Error reading variable information");
		close(fbfd);
		exit(3);
	}

	printf("%dx%d, %dbpp\n", vinfo.xres, vinfo.yres, vinfo.bits_per_pixel);

	framebuffer = malloc(vinfo.xres * vinfo.yres * sizeof(uint32_t));

	if(framebuffer == NULL) {
		perror("Error allocating temporary framebuffer");
	}

	screensize = vinfo.xres * vinfo.yres * vinfo.bits_per_pixel / 8;

	memset(framebuffer, 0, screensize);

	fbp = (char*) mmap(0, screensize, PROT_READ | PROT_WRITE, MAP_SHARED, fbfd, 0);

	if (fbp == -1) {
		perror("Error: failed to map framebuffer device to memory");
		free(framebuffer);
		munmap(fbp, screensize);
		close(fbfd);
		exit(4);
	}

	puts("The framebuffer device was mapped to memory successfully.");

	int frame_time = 0;
	int frame_number = 0;

	double target_fps = 30.0;
	double target_dt = 1.0 / target_fps;
	double current_time, last_time, delta_time;

	last_time = (double) clock() / CLOCKS_PER_SEC;

	while(1) {
		current_time = (double) clock() / CLOCKS_PER_SEC;

		delta_time = current_time - last_time;
		last_time = current_time;

		frame_number++;
		if(frame_number > 6574)
			frame_number = 1;
		load_frame(&frame, frame_number);

		for(int x = 0; x < 1440; x++) {
			for(int y = 0; y < 1080; y++) {
				framebuffer[(x + 240) + y * vinfo.xres] = frame[x + y * 1440];
			}
		}

		if(write(fbfd, framebuffer, screensize) != screensize) {
			perror("Error writing to framebuffer");
			close(fbfd);
			return 1;
		}

		lseek(fbfd, 0, SEEK_SET);

		double sleep_time = target_dt - delta_time;
		if(sleep_time > 0)
			usleep((useconds_t) (sleep_time * 1000000));
	}

	free(framebuffer);
	munmap(fbp, screensize);
	close(fbfd);

	return 0;
}
