Can be imported into eclipse ide (the variant for c/c++).

Put video frames into frames directory (must be in the directory that you run
the binary from) and run the program. If you're running a X11 windows server,
switch to a virtual console. I made this program for 1920x1080 screen size so
if your screen's or video's size vary, you have to correct the source code.

License: No license - do whatever you want.

---------
zielony12
